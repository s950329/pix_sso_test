<?php
class PixSSOLib
{
    /**
     * getSSOBlock 取得 PixSSO 所需的 js
     *
     * @static
     * @access public
     * @return none
     */
    public static function getSSOBlock()
    {
        $type = 2;
        $unique = crc32(uniqid());
        $now = time();
        $sig = crc32(PIXSSO_SECRET . $unique . $now);
?>
        <script type="text/javascript" src="//checklogin.<?= $_SERVER['SERVER_NAME'] ?>/login_name.php?key=<?= PIXSSO_KEY ?>&unique=<?= $unique ?>&timestamp=<?= $now ?>&sig=<?= $sig ?>"></script>
        <script type="text/javascript" src="//<?= PIXSSO_APISITE ?>/api/checklogin.php?js=1&unique=<?= $unique ?>&timestamp=<?= $now ?>&type=<?= $type ?>"></script>
        <script type="text/javascript" src="//<?= PIXSSO_PIXFSSITE ?>/js/pixnet/checklogin.min.js"></script>
<?php
    }

    /**
     * getSSOData 回傳 PixSSO 所需的資訊: login_name, API url
     *
     * @param string $user_name
     * @param array $options
     * @static
     * @access public
     * @return array
     */
    public static function getSSOData($user_name, $options = array())
    {
        $type = 0;
        $unique = crc32(uniqid());
        $now = time();

        $login_name = $user_name . '.pixnet.net' . $unique . $now;

        // add openid support
        if (array_key_exists('openid', $options)) {
            $login_name .= strval($options['openid']);
            $type |= 2;
        }

        $login_name = md5($login_name);
        $url = '//' . PIXSSO_APISITE . "/api/checklogin?js=jsonp&unique={$unique}&timestamp={$now}&type={$type}";
        return array($login_name, $url);
    }

    /**
     * getSSODataJSONP 回傳 JSONP 格式的 PixSSO 資訊
     *
     * @static
     * @access public
     * @return string
     */
    public static function getSSODataJSONP()
    {
        $user_name = $openid = '';
        if ($user = self::getLoginUser()) {
            $user_name = $user->user_name;
            $openid = $user->openid;
        }

        list($login_name, $url) = self::getSSOData($user_name, array(
            'openid' => $openid
        ));

        $data = array(
            'key' => PIXSSO_KEY,
            'login_name' => $login_name,
            'link' => $url
        );

        header('Content-Type: text/javascript');
        return htmlspecialchars($_GET['callback']) . '(' . json_encode($data) . ')';
    }

    /**
     * checkLogin 依指定登入的方式來判斷是否登入
     *
     * @param string $type, (pixnet|openid)
     * @static
     * @access public
     * @return mix
     */
    public static function checkLogin($type = 'pixnet')
    {
        if (!$pixdata = self::getLoginUser()) {
            return false;
        }

        if ('pixnet' == $type && $pixdata->user_name) {
            return $pixdata;
        }

        if ('openid' == $type && $pixdata->openid) {
            return $pixdata;
        }

        return false;
    }

    /**
     * getLoginUser 取得登入 user 的資訊
     *
     * @static
     * @access public
     * @return object
     */
    public static function getLoginUser()
    {
        $pixdata = isset($_COOKIE['pixdata']) ? json_decode($_COOKIE['pixdata']) : '';
        if (!$pixdata or !$pixdata->sig) {
            return NULL;
        }

        $sig = $pixdata->sig;
        unset($pixdata->sig);
        if ($sig === crc32(json_encode($pixdata) . $pixdata->nonce . PIXSSO_SECRET)) {
            return $pixdata;
        }

        return NULL;
    }

    /**
     * getLoginLink 取得登入網址
     *
     * @param string $done 完成後轉導的 url
     * @static
     * @access public
     * @return string
     */
    public static function getLoginLink($done = null)
    {
        if (is_null($done)) {
            $done = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        }
        return 'http://' . PIXSSO_MAINSITE . '/?done=' . urlencode($done);
    }

    /**
     * getLogoutLink 取得登出網址
     *
     * @param string $done 完成後轉導的 url
     * @static
     * @access public
     * @return string
     */
    public static function getLogoutLink($done = null)
    {
        if (is_null($done)) {
            $done = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        }
        return '//' . PIXSSO_PANELSITE . '/logout?done=' . urlencode($done);
    }

    /**
     * getOpenIDLoginLink 取得 openID 登入網址
     *
     * @param string $type openID provider
     * @param string $done 完成後轉導的 url
     * @param boolean $pixnet_only 如果帳號沒有連結到 pixnet user ，不要導到註冊頁
     * @static
     * @access public
     * @return string
     */
    public static function getOpenIDLoginLink($type, $done = null, $pixnet_only = true)
    {
        if (is_null($done)) {
            $done = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        }

        switch ($type) {
        case 'google':
            $openid = 'https://www.google.com/accounts/o8/id';
            $url = 'http://' . PIXSSO_PANELSITE . '/login/openid?done=' . urlencode($done) . '&openid=' . urlencode($openid);
            break;

        case 'facebook':
            $url = 'http://' . PIXSSO_PANELSITE . '/login/facebooklogin?done=' . urlencode($done);
            break;

        case 'yahoo':
            $openid = 'https://me.yahoo.com/';
            $url = 'http://' . PIXSSO_PANELSITE . '/login/openid?done=' . urlencode($done) . '&openid=' . urlencode($openid);
            break;

        case 'live':
            $url = 'http://' . PIXSSO_PANELSITE . '/login/livelogin?done=' . urlencode($done);
            break;

        case 'gamebase':
            $openid = 'http://www.gamebase.com.tw/service/openid.php';
            $url = 'http://' . PIXSSO_PANELSITE . '/login/openid?done=' . urlencode($done) . '&openid=' . urlencode($openid);
            break;
        }

        if (!$pixnet_only) {
            $url .= '&without_signup=1';
        }
        return $url;
    }
}
