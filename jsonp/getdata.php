<?php
// 這個的功用是用 jsonp 回傳一個 login_name 以及 link ，讓 js 可以判斷 sso.pixplug.in 記錄的身份是否與 api.pixnet.cc 相同
require_once(__DIR__ . '/../bootstrap.php');

echo PixSSOLib::getSSODataJSONP();
