<?php
require_once(__DIR__ . '/../bootstrap.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<style type="text/css">
pre {
    background: #EEEEEE;
    padding: 5px;
}
</style>
</head>
<body>
<h2>jsonp version</h2>
<p><a href="/">normal version</a></p>
<hr>

ps. 須透過 normal version 來作登入登出動作
<pre>
access log (/tmp/pixSSO-jsonp-webbertest):
<?= `tail /tmp/pixSSO-jsonp-webbertest` ?>
<?php print_r(pixSSOLib::getLoginUser()) ?>
</pre>
<hr>

<ul>
    <li>/index.php <a href="index.phps" target="_blank">原始檔</a> # javascript 動態載入 checklogin.js，可自行調整 checklogin.js 的路徑 (範例: http://sso.pixplug.in/jsonp/checklogin.js)</li>
    <li>/checklogin.js <a href="checklogin.js" target="_blank">原始檔</a></li>
    <li>/getdata.php <a href="getdata.phps" target="_blank">原始檔</a> # 於 checklogin.js 指定路徑 (可自行調整路徑，範例: http://sso.pixplug.in/jsonp/getdata.php)</li>
    <li>/done.php <a href="done.phps" target="_blank">原始檔</a> # 於 checklogin.js 指定路徑 (可自行調整路徑，範例: http://sso.pixplug.in/jsonp/done.php)<br></li>
    <li>/PixSSOLib.php <a href="/PixSSOLib.php" target="_blank">原始檔</a> # 上述的 .php 皆需載入此檔 (為避免重複的 code，範例統一在 bootstrap.php 載入)</li>
</ul>
<hr>

<script type="text/javascript"><!--
var js = document.createElement('script');
js.src = <?= '"//' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'] . 'checklogin.js"' ?>;
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(js, s);
//--></script>
</body>
</html>
