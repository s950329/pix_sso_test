<?php
require_once(__DIR__ . '/../bootstrap.php');

if ($user = PixSSOLib::checkLogin('pixnet')) {
    $type = 'pixnet';
    $user_displayname = $user->user_displayname;
} elseif ($user = PixSSOLib::checkLogin('openid')) {
    $type = $user->openid_type;
    $user_displayname = $user->openid_data->user_displayname;
} else {
    exit;
}

$content = sprintf("%s, %s login by %s\n", date('Y-m-d H:i:s', time()), $user_displayname, $type);
file_put_contents('/tmp/pixSSO-jsonp-webbertest', $content, FILE_APPEND);
