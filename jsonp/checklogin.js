(function(){
    var jsonp = function(url, callback, callback_fn){
        var fn;
        if ('undefined' == typeof(callback_fn)) {
            fn = 'cb' + Math.floor(10000000 * Math.random());
            url += ((url.indexOf('?') > 0) ? '&' : '?') + 'callback=' + encodeURIComponent(fn);
        } else {
            fn = callback_fn;
        }

        window[fn] = function(ret){
            callback(ret);
        };
        var js = document.createElement('script');
        js.src = url;
        document.getElementsByTagName('head')[0].appendChild(js);
    };

    var domain_append = '';

    var server_name = document.location.host;
    var nodes = server_name.split('.');
    if (nodes[nodes.length - 1] == 'pixnet') {
        domain_append = '.' + nodes[nodes.length - 3] + '.' + nodes[nodes.length - 2] + '.' + nodes[nodes.length - 1];
    }

    var site = 'pinxet.test:3000' + domain_append;
    var api_site = 'api.pixnet.cc' + domain_append;

    var checklogin_done = function(){
        var img = new Image();
        img.src = '//' + site + '/jsonp/done.php';
    };

    jsonp('//' + site + '/jsonp/getdata.php', function(ret){
        var key = ret.key;
        var api_link = ret.link;
        var analytics_loginname = ret.login_name;
        jsonp(api_link, function(ret){
            if (ret.panel_login_name == analytics_loginname) {
                checklogin_done();
                return;
            }

            var callback = 'checklogin_' + Math.floor(Math.random() * 10000);
            var url = '//' + api_site + '/api/checklogin?version=2&done=' + encodeURIComponent('http://checklogin.' + site + '/getssodata.php?js=jsonp&callback=' + callback + '&key=' + key);
            jsonp(url, function(ret){
                checklogin_done();
            }, callback);
        });
    });
})();
