<?php
header('P3P:CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
require_once('bootstrap.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<style type="text/css">
pre {
    background: #EEEEEE;
    padding: 5px;
}
</style>
<?= PixSSOLib::getSSOBlock() ?>
</head>
<body>
<h2>normal version</h2>
<p><a href="/jsonp/">jsonp version</a></p>
<hr>
<?php if ($user = PixSSOLib::checkLogin('pixnet')) { ?>
<?= $user->user_name ?>, <a href="<?= PixSSOLib::getLogoutLink() ?>">logout</a>
<?php } elseif ($user = PixSSOLib::checkLogin('openid')) { ?>
<?= $user->openid ?> (<?= $user->openid_type ?>),<a href="<?= PixSSOLib::getLogoutLink() ?>">logout</a>
<br>
<?php } else { ?>
guest, <a href="<?= PixSSOLib::getLoginLink() ?>">login</a>
<a href="<?= PixSSOLib::getOpenIDLoginLink('google', null, false) ?>">Google登入</a>
<a href="<?= PixSSOLib::getOpenIDLoginLink('facebook', null, false) ?>">Facebook登入</a>
<a href="<?= PixSSOLib::getOpenIDLoginLink('yahoo', null, false) ?>">Yahoo登入</a>
<a href="<?= PixSSOLib::getOpenIDLoginLink('live', null, false) ?>">live登入</a>
<a href="<?= PixSSOLib::getOpenIDLoginLink('gamebase', null, false) ?>">gamebase登入</a>
<?php } ?>
<pre>
<?php print_r($user) ?>
</pre>
<hr>
/index.php <a href="index.phps" target="_blank">原始檔</a><br>
/PixSSOLib.php <a href="PixSSOLib.php" target="_blank">原始檔</a>
<hr>
</body>
</html>
